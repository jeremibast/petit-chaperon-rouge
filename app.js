// Up and down fonts size

document.querySelector("#fontSize").addEventListener("input", function () {
    document.body.style.fontSize = this.value + "%";
});

// Replace an element in page

function replace() {

    // Select an element to replace
    var input = document.getElementById("in").value;
    // replacement element
    var output = document.getElementById("out").value;
    // Regex
    var regex = new RegExp(input, "g");
    // Replace in page
    document.getElementById("page").innerHTML = document.getElementById("page").innerHTML.replace(regex, output);

}

// Dark theme

const btn = document.querySelector(".btn-toggle");
const prefersDarkScheme = window.matchMedia("(prefers-color-scheme: dark)");
const currentTheme = localStorage.getItem("theme");

if (currentTheme == "dark") {
    document.body.classList.toggle("dark-theme");
} else if (currentTheme == "light") {
    document.body.classList.toggle("light-theme");
}

btn.addEventListener("click", function () {
    if (prefersDarkScheme.matches) {
        document.body.classList.toggle("light-theme");
        var theme = document.body.classList.contains("light-theme")
            ? "light"
            : "dark";
    } else {
        document.body.classList.toggle("dark-theme");
        var theme = document.body.classList.contains("dark-theme")
            ? "dark"
            : "light";
    }
    localStorage.setItem("theme", theme);
});

